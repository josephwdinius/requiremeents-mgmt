## This project is available as a demonstration for the current Requirements Management functionality.

### Current Implementation

This project currently demonstrates the functionality contained within the GitLab 13.8 self-managed release.

What this project simulates is a situation where there are two tests which run and requirements are given a status of `passed` or `failed` based on test output. The requirements are linked to tests through the use of a [requirements trace matrix][trace-matrix]. 

1. Tests are run in the `test` stage of the CI/CD pipeline
    - The results files (`*.results`) are stored as artifacts
    - Thes artifacts are passed by default from the `test` stage of the pipeline to the `requirements_check` stage of the pipeline
1. The `requirements_check` stage of the pipeline is executed only if the commit was to the `master` branch.
    - This runs a simple python script ([parse_coverage.py][parse-script]) which utilzes the [requirements trace matrix][trace-matrix] to correlate test pass / fail to the requirements
    - Based on this correlation, the [python script][parse-script] will output a `.json` file in the appropriate format (below) to pass to the [requirements report][req-report] feature of the CI/CD pipeline
    - When the requirements report is run, the project requirements will be updated with the `satisfied` / `failed` badges as appropriate.

### JSON Format

The [Requirement Report][req-report] expects properly formatted JSON in the following format:

```
{
    "6":"passed",
    "8":"failed"
}
```

The above example will shows that testing for  `REQ-6` has passed, and therefore `REQ-6` should be marked as `Satisfied` in the requirement list. It also shows that testing for `REQ-8` is failing, and therefore `REQ-8` should be marked as `failed` in the Requirement list.


### Future Updates

* Better describe the options in the `.gitlab-ci.yml` file
* ~~Rename results files as .json for clarity~~
* ~~Update demo to use a requirements trace matrix (.csv file) for more real-world exmaple~~

[trace-matrix]: /artifacts/trace-matrix.csv
[parse-script]: /verification/parse_coverage.py
[req-report]: https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsrequirements
